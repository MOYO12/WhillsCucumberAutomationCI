package comWhillsRide;

import comWhillsBase.BaseWhills;
import comWhillsPages.WhillsHomePage;
import comWhillsPages.WhillsLandingPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

/**
 * Created by User on 17/04/2017.
 */
public class WhillsStepDefin extends BaseWhills{
    public WebDriver driver;

            public WhillsStepDefin(){
              this.driver = WhillsHOOK.driver;
             }
//
//                 @Given("^That am on the homme page and click login$")
//             public void that_am_on_the_homme_page_and_click_login() throws Throwable {
//             driver.get("https://www.williamhill.com");
//    }
//
//          @Then("^am able to navigate througn the website and click on join now$")
//       public void am_able_to_navigate_througn_the_website_and_click_on_join_now() throws Throwable {
//              WhillsHomePage WHP = new WhillsHomePage(driver);
//              WHP.Access();
//    }
//
//            @When("^am able to supply all the necessary credentials to register for an account$")
//    public void am_able_to_supply_all_the_necessary_credentials_to_register_for_an_account() throws Throwable {
//                WhillsLandingPage WCA = new WhillsLandingPage(driver);
//                WCA.Register();
//    }
//
//             @Then("^am able to accept terms and condition and click on create account$")
//    public void am_able_to_accept_terms_and_condition_and_click_on_create_account() throws Throwable {
//
//    }
//


    @Given("^That am on the home page and i click on betting$")
    public void that_am_on_the_home_page_and_i_click_on_betting() throws Throwable {
        driver.get("https://www.williamhill.com");
        WhillsHomePage WHP = new WhillsHomePage(driver);
        WHP.Access();
    }

    @Then("^I click on football$")
    public void i_click_on_football() throws Throwable {
        WhillsLandingPage WLP = new WhillsLandingPage(driver);
        WLP.Register();

    }

    @When("^I click on daily match list to see all match displayed$")
    public void i_click_on_daily_match_list_to_see_all_match_displayed() throws Throwable {
        WhillsLandingPage WLP = new WhillsLandingPage(driver);
        WLP.Register();
    }

    @Then("^Am able to click on a match of my choice to see market$")
    public void am_able_to_click_on_a_match_of_my_choice_to_see_market() throws Throwable {
        WhillsLandingPage WLP = new WhillsLandingPage(driver);
        WLP.Register();
    }

    @When("^Am able to click on tenis$")
    public void am_able_to_click_on_tenis() throws Throwable {
        WhillsLandingPage WLP = new WhillsLandingPage(driver);
        WLP.Register();
    }

    @Then("^Am able to click on matches to see all displayed matches$")
    public void am_able_to_click_on_matches_to_see_all_displayed_matches() throws Throwable {
        WhillsLandingPage WLP = new WhillsLandingPage(driver);
        WLP.Register();
    }


}
