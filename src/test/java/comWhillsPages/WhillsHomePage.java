package comWhillsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 17/04/2017.
 */
public class WhillsHomePage {

    public WebDriver driver;

          public WhillsHomePage(WebDriver driver){
              this.driver = driver;
              PageFactory.initElements(driver,this);
          }
          @FindBy(how = How.XPATH,using = "/html/body/div[3]/div/div/div[2]/wf-header/header/div/div[1]/div[2]/div/nav/div/div/a[1]/span")
          public static WebElement Betting;


          public void Access(){
              Betting.click();
          }

}
