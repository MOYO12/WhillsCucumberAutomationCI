package comWhillsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 18/04/2017.
 */
public class WhillsLandingPage {

    public WebDriver driver;

         public WhillsLandingPage(WebDriver driver){
             this.driver = driver;
             PageFactory.initElements(driver,this);

         }
           @FindBy(how = How.ID,using = "football")public static WebElement Football;
         @FindBy(how = How.ID,using = "10810040_mkt_namespace")public static WebElement MatchChoice;
         @FindBy(how = How.ID,using = "tennis")public static WebElement Tennis;
         @FindBy(how = How.ID,using = "10952804_mkt_namespace")public static WebElement TennisMatchChoice;







         public void Register()  {
             Football.click();
             MatchChoice.click();
             Tennis.click();
             TennisMatchChoice.click();

         }
  }
